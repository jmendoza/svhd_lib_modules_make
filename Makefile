
define mtree_func
$(VTREE) mtree $(LIB_ROOT_PATH) $(1) -v 1;
endef

define minsts_func
$(VTREE) minsts $(LIB_ROOT_PATH) $(1) -v 1;
endef

define print_src
echo " [+] $(1)";
endef
docs:
	$(TEROSHDL_DOC) -i src --outpath doc --symbol_verilog "!" --self_contained

mtree:
	@($(call print_src,"LIB_PATH: "$(LIB_ROOT_PATH)))
	@($(foreach TOP_MODULE,$(MODULES_LIST), $(call mtree_func,$(TOP_MODULE))))

minsts:
	@($(call print_src,"LIB_PATH: "$(LIB_ROOT_PATH)))
	@($(foreach TOP_MODULE,$(MODULES_LIST), $(call minsts_func,$(TOP_MODULE))))


clean:
	rm -f $(DOC_FOLDER)/*.html
	rm -f *.dep




  
    